package com.foxminded.cleancode.collectionframework.charcounter;

import java.util.LinkedHashMap;
import java.util.Map;

import com.foxminded.cleancode.collectionframework.Countable;

public class CachedCharCounter implements Countable {
	
	private Map<String, Map<Character, Integer>> cache = new LinkedHashMap<>();
	private Countable countable;
	
	public CachedCharCounter(Countable countable) {
		this.countable = countable;
	}

	@Override
	public Map<Character, Integer> count(String line) {
		if (cache.containsKey(line)) {
			return cache.get(line);
		}
		Map<Character, Integer> countedString = countable.count(line);
		cache.put(line, countedString);
		return countedString;
	}
}
