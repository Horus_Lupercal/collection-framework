package com.foxminded.cleancode.collectionframework.charcounter;

import java.util.LinkedHashMap;
import java.util.Map;

import com.foxminded.cleancode.collectionframework.Countable;

public class CharCounter implements Countable {

	public Map<Character, Integer> count(String line) {
		Map<Character, Integer> map = new LinkedHashMap<>();
		char[] chars = line.toCharArray();

		for (char oneChar : chars) {
			if (map.containsKey(oneChar)) {
				map.put(oneChar, map.get(oneChar) + 1);
			} else {
				map.put(oneChar, 1);
			}
		}
		return map;
	}

}
