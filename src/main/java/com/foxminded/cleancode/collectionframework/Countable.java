package com.foxminded.cleancode.collectionframework;

import java.util.Map;

public interface Countable {
	
	Map<Character, Integer> count(String line);
}
