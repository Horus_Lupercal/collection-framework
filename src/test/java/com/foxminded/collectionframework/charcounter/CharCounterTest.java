package com.foxminded.collectionframework.charcounter;

import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.foxminded.cleancode.collectionframework.charcounter.CharCounter;

public class CharCounterTest {

	private CharCounter charCounter = new CharCounter();

	@Test
	public void testCounterCorrectStringValueExceptedIsOk() {

		String value = "TTT ttt sss 22!! %";

		Map<Character, Integer> result = charCounter.count(value);
		
		Map<Character, Integer> exceptedResult = getMap();;
		
		Assertions.assertEquals(result, exceptedResult);
	}
	
	private Map<Character, Integer> getMap() {
		Map<Character, Integer> map = new LinkedHashMap<>();
		map.put('T', 3);
		map.put(' ', 4);
		map.put('t', 3);
		map.put('s', 3);
		map.put('2', 2);
		map.put('!', 2);
		map.put('%', 1);
		return map;
	}
}
