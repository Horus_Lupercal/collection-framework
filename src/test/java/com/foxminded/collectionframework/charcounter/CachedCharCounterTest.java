package com.foxminded.collectionframework.charcounter;

import java.util.Map;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.foxminded.cleancode.collectionframework.charcounter.CachedCharCounter;
import com.foxminded.cleancode.collectionframework.charcounter.CharCounter;

public class CachedCharCounterTest {

	private CachedCharCounter cache = new CachedCharCounter(new CharCounter());
	
	@Test
	public void testCashDifferentStringValueExceptedDifferentReference() {
		String firstString = "111 222 333 !!! sDs";
		Map<Character, Integer> fistStringResult = cache.count(firstString);
		
		String secondString = "123 225 335";
		Map<Character, Integer> secondStringResult = cache.count(secondString);
		
		Assertions.assertFalse(fistStringResult == secondStringResult);
	}
	
	@Test
	public void testCashSameStringValueExceptedSameReference() {
		String firstString = "111 222 333 !!! sDs";
		Map<Character, Integer> fistStringResult = cache.count(firstString);
		
		String secondString = "111 222 333 !!! sDs";
		Map<Character, Integer> secondStringResult = cache.count(secondString);
		
		String thirdString = "qwerty";
		Map<Character, Integer> thirdStringResult = cache.count(thirdString);
		
		String fourthString = "qwerty";
		Map<Character, Integer> fourthStringResult = cache.count(fourthString);
		
		Assertions.assertTrue(fistStringResult == secondStringResult);
		Assertions.assertTrue(thirdStringResult == fourthStringResult);
	}
}
